var ehandlebars = require('express-handlebars'),
  moment = require('moment');

module.exports = function(app) {
  var hbs = ehandlebars.create({
    defaultLayout: 'app',
    //partialsDir: ['partials'],
    helpers: {
      section: function(name, options) {
        if (!this._sections) this._sections = {}
        this._sections[name] = options.fn(this)
        return null
      },

      formatDate(date, format, options){
        if(format.constructor === Object){
          options = format;
          format = 'YYYY-MM-DD';
        }
        return moment(date).format(format);
      },

      formatDuration(duration, format, options){
        var val = moment.duration(duration, 'seconds')[format]();
        if(format === 'minutes' && val < 10){
          return '0' + val;
        }else{
          return val;
        }
      },

      multiply(val1, val2){
        return val1 * val2;
      },

      ifEqual(oneString, twoString, options){
         if(oneString == twoString){
          return options.fn(this)
        } else {
          return options.inverse(this)
        }
      }
    }
  })

  app.engine('handlebars', hbs.engine)
  app.set('view engine', 'handlebars')
}