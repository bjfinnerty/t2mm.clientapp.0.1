mixpanel = require('mixpanel').init("f3ef65042b731f802ab233a949f7a7e5");

module.exports.logUserLogin = function(user){
	mixpanel.people.increment(user.email, 'app_visits');
	mixpanel.people.set('last_sign_in', (new Date()).toISOString());
}

module.exports.logUserLogout = function(user){
	mixpanel.people.set('last_sign_out', (new Date()).toISOString());
}

module.exports.logSignup = function(user){
	mixpanel.people.set(user.email, {
	    "$email": user.email,
	    "app_visits": 1,
	    "$last_login": new Date()
	});
}