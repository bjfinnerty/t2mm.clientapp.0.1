var Sequelize = require('sequelize');

if(process.env.NODE_ENV === 'local'){
	module.exports = new Sequelize('postgres://localhost:5432/ttmm_dev');
}else if(process.env.NODE_ENV === 'test'){
	module.exports = new Sequelize('postgres://localhost:5432/ttmm_test');
}else if(process.env.NODE_ENV === 'tunnel'){
	module.exports = new Sequelize('t2mm_dev1', 'brian', 'brian', {
	  host: 'localhost',
	  dialect: 'postgres',
	  port: 9001,
	  pool: {
	    max: 5,
	    min: 0,
	    idle: 10000
	  }
	});
}else{
	module.exports = new Sequelize('t2mm_dev1', 'brian', 'brian', {
	  host: 'localhost',
	  dialect: 'postgres',

	  pool: {
	    max: 5,
	    min: 0,
	    idle: 10000
	  }

	});
}