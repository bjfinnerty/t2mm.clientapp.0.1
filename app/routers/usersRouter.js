var passport = require('passport'),
    userController = require('../controllers/userController.js'),
    isAuthenticated = require('../utils/isAuthenticated.js'),
    mixpanel = require('../utils/mixpanel.js');

module.exports = function(express) {
    var router = express.Router();

    // Log in / out routes
    router.get('/users/login/', userController.showLogin);
    router.post('/users/login/', passport.authenticate('local', {
        failureRedirect: '/users/login/',
        failureFlash: true 
    }), function(req, res, next) {
            req.session.save(function (err) {
                if(err){
                    return next(err);
                }else{
                    mixpanel.logUserLogin(req.user);
                    res.redirect('/');
                }
            });
        });


    router.get('/users/logout', userController.logout);


    // Sign up routes
    router.get('/users/signup/', userController.show);
    router.post('/users/signup/', userController.signup);

    // Account
    router.get('/users/account/', isAuthenticated, userController.showAccount);
    router.post('/users/account/', isAuthenticated, userController.updateAccount);

    return router
}

