var passport = require('passport'),
    adminController = require('../controllers/adminController.js'),
    isAuthenticated = require('../utils/isAuthenticated.js');   

module.exports = function(express) {

	var router = express.Router();

	router.get('/admin/dbdump', adminController.dbDump);
	router.get('/admin/dbfetch', adminController.dbFetch);

	return router
};