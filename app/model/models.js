var UserMeta = require('./User.js'),
	FeedbackMeta = require('./Feedback.js'),
	RecordingMeta = require('./Recording.js'),
    StatsDailyMeta = require('./StatsDaily.js'),
    connection = require('../utils/sequelize.js'); 

var User = connection.define('users', UserMeta.attributes, UserMeta.options);
var Feedback = connection.define('feedback', FeedbackMeta.attributes, FeedbackMeta.options);
var Recording = connection.define('recording', RecordingMeta.attributes, RecordingMeta.options);
var StatsDaily = connection.define('stats_daily', StatsDailyMeta.attributes, StatsDailyMeta.options);

// you can define relationships here

module.exports.User = User;
module.exports.Feedback = Feedback;
module.exports.Recording = Recording;
module.exports.StatsDaily = StatsDaily;
module.exports.connection = connection;