var Sequelize = require('sequelize'),
	User = require('./User.js');

var attributes = {

	user_id: {
		type: Sequelize.INTEGER,
		references: {
			model: User,
			key: 'id',
			deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE
		}
	},

    week_no: {
		type: Sequelize.INTEGER
	},

	date : {
		type: Sequelize.DATE
	},

	process_day : {
		type: Sequelize.DATE
	},

	word_count: {
		type: Sequelize.INTEGER
	},

	avg_words_per_hour: {
		type: Sequelize.INTEGER
	},

	top_ten_words: {
		type: Sequelize.STRING(1000)
	},

	top_ten_themed: {
		type: Sequelize.STRING(1000)
	},

	total_rec_time:{
		type: Sequelize.INTEGER
	}

};

var options = {
  freezeTableName: true
};

module.exports.attributes = attributes;
module.exports.options = options;