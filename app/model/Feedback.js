var Sequelize = require('sequelize'),
	User = require('./User.js');

var attributes = {

	user_id: {
		type: Sequelize.INTEGER,
		references: {
			model: User,
			key: 'id',
			deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE
		}
	},
  
	comments: {
		type: Sequelize.STRING(1000)
	}
  
}

var options = {
  freezeTableName: true
}

module.exports.attributes = attributes;
module.exports.options = options;