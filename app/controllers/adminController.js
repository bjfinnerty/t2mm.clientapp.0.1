var Model = require('../model/models.js'),
	moment = require('moment'),
	_ = require('lodash'),
    request = require('request-promise');

module.exports.dbDump = function(req, res, next) {
    
    var excludeIds = [-1].concat(JSON.parse(req.query.ids) || []);

	var recordings = Model.Recording.findAll({
		'where':{
			'id' : {
				'$notIn' : excludeIds
			}
		}
	}).then(function(data){
		return res.json({
			'recordings': data
		})
	});
};


module.exports.dbFetch = function(req, res, next) {
    
    var host = req.query.host || 'localhost:3000'

    var getExcludeIds = function(){
        return Model.Recording.findAll({
            'attributes' : ['id']
        });
    };

    var dataRequest = function(models){
        var excludeIds = models.map(function(mod){
            return mod.id
        });
        return request.get({
            'uri': 'http://' + host + '/admin/dbdump/',
            'qs' : {'ids':JSON.stringify(excludeIds)},
            'headers': {
                'content-type': 'application/json'
            }
        });
    };

    var insertRecordings = function(recordings){
        var dataAsJSON = JSON.parse(recordings);
        return Model.Recording.bulkCreate(dataAsJSON.recordings);
    };

    getExcludeIds()
    .then(dataRequest)
    .then(insertRecordings)
    .then(function(updatedRecordings){
        return res.json(updatedRecordings);
    })
    .catch(function(err){
        next(err);
    });

};