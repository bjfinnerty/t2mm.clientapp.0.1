var bcrypt = require('bcrypt'),
    Model = require('../model/models.js'),
    _ = require('lodash'),
    moment = require('moment'),
    passport = require('passport'),
    mixpanel = require('../utils/mixpanel.js');

module.exports.show = function(req, res, next) {
  res.render('signup', {layout: 'body'});
};


function checkUserExists(email){

    return Model.User.findOne({
        where: {
            'email': email
        }
    }).then(function (user) {
        if(user){
            throw('There is a user with that email already.');
        }
        return user;
    }).catch(function(err){       
        throw(err);
    });
}

function createNewUser(newUser){

    return Model.User.create(newUser)
    .then(function(user) {
        mixpanel.logSignup(user);
        return user;
    }).catch(function(error) {
        throw("Please, choose a different email.");
    })
}


module.exports.signup = function(req, res, next) {

    var email = req.body.email;
    var password = req.body.password;
    var password2 = req.body.password2;
  
    if (!email || !password || !password2) {
        req.flash('error', "Please, fill in all the fields.");
        res.redirect('/users/signup/')
    }
  
    if (password !== password2) {
        req.flash('error', "Please, enter the same password twice.");
        res.redirect('/users/signup/')
    }

    var salt = bcrypt.genSaltSync(10);
    var hashedPassword = bcrypt.hashSync(password, salt);

    var newUser = {
        email: email,
        salt: salt,
        password: hashedPassword,
        activation_date: new Date()
    };
  

    return checkUserExists(email)
    .then(function(){
        return createNewUser(newUser);
    })
    .then(function(user){
        req.session.passport = {user:user.id};
        passport.authenticate('local', function(err, user) {
            if (!user) { 
                return res.redirect('/login'); 
            }else{
                req.session.save(function (err) {
                    if(err){
                        req.flash('error', err.msg);
                        res.redirect('/users/signup/');
                    }else{
                        req.flash('info', 'Welcome to Talk To Me More. Please fill in the fields below and we\'ll get started');
                        res.redirect('/users/account');
                    }
                  });
            }
        })(req, res, next);
    })
    .catch(function(errMsg){
        req.flash('error', errMsg);
        res.redirect('/users/signup/');
    })


  
};

module.exports.showLogin = function(req, res, next) {
    res.render('login', {layout: 'body'});
};


module.exports.logout = function(req, res) {
    req.logout();
    res.redirect('/');
};

module.exports.showAccount = function(req, res) {
    var user = req.user;
    user.child_dob_string = moment(user.child_dob).format('YYYY-MM-DD');
    res.render('account', {user:user});
};

module.exports.updateAccount = function(req, res, next) {

    var user = req.user;

    var updatedData = _.merge(req.body, {
        mothers_contact_no : convertToNum(req.body.mothers_contact_no),
        fathers_contact_no : convertToNum(req.body.fathers_contact_no),
        child_dob : convertToDate(req.body.child_dob),
        activation_date: convertToDate(req.body.activation_date)
    });

    var validData = _.pick(updatedData, function(v, k) {
        return !!v;
    });

    user.update(validData)
      .then(function(user){
          return res.render('account', {user:user});
      })
      .catch(function(err){
          next(err);
      });
 
};

var convertToNum = function(string){
    var newNum = parseInt(string, 10);
    if(_.isNaN(newNum)){
      return null;
    }else{
      return newNum;
    }
};

var convertToDate = function(dateString){
    var newDate = Date.parse(dateString);
    if(_.isNaN(newDate)){
      return null;
    }else{
      return new Date(newDate);
    }
};

