var Model = require('../model/models.js'),
	RecordingQueries = require('../sql/recording.js'),
	moment = require('moment'),
	_ = require('lodash');

/*
*	Common Functions
*/

var getDashboardDataFromRecording = function(userId, startDate, endDate){

	var query = RecordingQueries.dateRangeWordCount(userId, startDate, endDate);
	var sequelize = Model.connection;

	return sequelize.query(query, { 
		type: sequelize.QueryTypes.SELECT
	})
	.then(function(counts) {
    	return counts;
    })
	.catch(function(err){
		throw err;
	});
};

var getDashboardDataFromStatsDaily = function(userId, startDate, endDate){

	return Model.StatsDaily.findAll({
        //attributes: ['word_count', ['date', 'day']],
        where : {
            user_id : userId,
            date: {
                $lte: endDate,
                $gt: startDate
            }
        },
        order: ['date']
    }).then(function(data){
        return data;
    })
    .catch(function(err){
        throw err;
    });
};

var getWeekRange = function(current_week){

	if(current_week){
		return Array.apply(null, {length: current_week - 1}).map(function(nada, i){return i+1});
	}else{
		return [];
	}
}

var getWeekData = function(urlWeek, currentWeek){
	
	return {
		past: getWeekRange(currentWeek), 
		current:currentWeek, 
		active:parseInt(urlWeek, 10) || currentWeek
	}
}

var formatForC3 = function(data){
	
	if (!data.length) {
		return null;
	}else{
		var xData = ['x'],
			countData = ['Word Count'];

		data.forEach(function(day){
  			xData.push(moment(day.date).format('YYYY-MM-DD').toString());
  			countData.push(day.word_count)
		});

		return JSON.stringify([xData, countData]);
	}
}
/*
*	Router functions
* 		- Dashboard
*/


module.exports.redirectToDash = function(req, res, next) {
    res.redirect('/dashboard/');
};


module.exports.showDashboard = function(req, res, next) {

	var weekData = getWeekData(req.params.week, req.user.current_week);
	weekData.root = 'dashboard';
	var startDate = moment(req.user.activation_date).add(weekData.active, 'week').subtract(7, 'days');
	var endDate = moment(req.user.activation_date).add(weekData.active, 'week');
	
	getDashboardDataFromStatsDaily(req.user.id, startDate.toDate(), endDate.toDate())
	.then(function(data) {

		var columns = formatForC3(data);
        var counts = data.map(function(obj){
            return {
                'word_count': obj.word_count,
                'day' : obj.date
            }
        });

        var ttw = getTopTens(data);

        //var topTenWords = getWeeklyWordCount(_.pluck(data, 'top_ten_words'));

    	res.render('dashboard', {
			    'user' : req.user,
			    'counts': counts.length ? JSON.stringify(counts) : null,
			    'c3Columns': columns,
			    'startDate' : startDate.format('DD/MM/YYYY'),
			    'endDate' : endDate.format('DD/MM/YYYY'),
			    'weekData' : weekData,
			    'data': data,
			    'ttw' : ttw
			});	
		})
	.catch(function(err){
		next(err);
	});
};



/*
*	Programme
*/

module.exports.showProgramme = function(req, res, next){

	var weekData = getWeekData(req.params.week, req.user.current_week);
	weekData.root = 'programme';

	res.render('programme/week-' + weekData.active, {
			'user' : req.user,
			'weekData': weekData
		},
		function(err, html){
			if(err){
				if(err.message.indexOf('Failed to lookup view') > -1){
					res.render('programme/week-0', {
						'user' : req.user,
						'weekData': weekData
					});
				}else{
					next(err);
				}
			}
			else{
				res.send(html);
			}
		}
	);
};



/*
* 	Feedback
*/

module.exports.showFeedback = function(req, res, next) {

	return Model.Feedback.findAll()
		.then(function(comments){
			res.render('feedback', {
			    'user' : req.user,
			    'previous_comments': comments
			});	
		})
		.catch(function(err){
			console.log('====errr =======')
			next(err);
		});
};


module.exports.addFeedback = function(req, res, next) {

	var newComment = {   
	    user_id: req.user.id,
	    comments: req.body.comment
	};

	return Model.Feedback.create(newComment)
		.then(function(comment){
			req.flash('success', "Comment Received! Thank you very much for your feedback.");
			res.redirect('/feedback/');
		})
		.catch(function(err){
			next(err);
		});
};


if (process.env.NODE_ENV === 'test') {
  	module.exports._private = { 
  		'getWeekData': getWeekData,
  		'getWeekRange': getWeekRange 
  	};
}


var getTopTens = function(data){

	var allCounts = data.map(function(obj){
		return obj.top_ten_words
	}).join(',').split(',');

	var countMap = {};

	allCounts.forEach(function(wordAndCount){
		if(wordAndCount.indexOf(':') > -1){
			var word = wordAndCount.split(':')[0];
			var count = parseInt(wordAndCount.split(':')[1], 10);

			if(countMap[word]){
				countMap[word] += count;
			}else{
				countMap[word] = count;
			}
		}
	});

	var nestedArray = _.sortBy(_.pairs(countMap), function(arr){
		return  - arr[1]
	}).slice(0, 10);

	return JSON.stringify(nestedArray);
}
