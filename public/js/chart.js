
var Chart = function(){

  var _this = this;

  var scaleX = function(data, width){
    return d3.scale.ordinal().domain(data.map(function(d) { 
      return d.day 
    })).rangeRoundBands([0, width], .5);
  }

  var scaleY = function(data, height){
    return d3.scale.linear().domain([0, d3.max(data, function(d) { return d.word_count; })]).range([height, 0]);
  }

  var getXAxis = function(data, x){
    return d3.svg.axis()
      .scale(x)
      .orient("bottom")
      .tickFormat(function(d) { return moment(d).format('ddd Do'); });
  }

  var getYAxis = function(data, y){
    return d3.svg.axis()
      .scale(y)
      .orient("left")
      .ticks(4);
    }


  var type = function(d) {
    d.word_count = +d.word_count; // coerce to number
    return d;
  }

  this.render = function(data){

    var margin = {top: 20, right: 30, bottom: 30, left: 40},
      width = jQuery('.chart-wrapper').width() - margin.left - margin.right,
      height = 300 - margin.top - margin.bottom;

      var x = scaleX(data, width),
        y = scaleY(data, height),
        xAxis = getXAxis(data, x),
        yAxis = getYAxis(data, y);

      var chart = d3.select(".chart")
        .attr("width", width)
        .attr("height", height)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

      chart.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + height + ")")
        .attr("stroke", "#000000")
        .call(xAxis);

      chart.append("g")
          .attr("class", "y axis")
          .attr("stroke", "#000000")
          .call(yAxis)
          .append("text")
          .style("text-anchor", "end")

      chart.selectAll(".bar")
            .data(data)
            .enter().append("rect")
            .attr("class", "bar")
            .attr("x", function(d) { return x(d.day); })
            .attr("y", function(d) { return y(d.word_count); })
            .attr("height", function(d) { return height - y(d.word_count); })
            .attr("width", x.rangeBand());

    return this;
  }

  this.rerender = function(data){
    
      d3.select(".chart").selectAll('*').remove();
      this.render(data);
  }

  return this;
}
