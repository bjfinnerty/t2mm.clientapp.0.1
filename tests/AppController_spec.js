controller = require('../app/controllers/appController'),
	chai = require('chai'),
	sinon = require('sinon'),
	expect = chai.expect,
	Model = require('../app/model/models.js');


// before(function(){

// 	Model.Feedback.sync({force: true}).then(function () {
// 	  // Table created
// 	  // return Model.Feedback.create({
// 	  //   user_id: 1,
// 	  //   comments: 'Initial comment'
// 	  // });

// 	console.log(arguments);
// 	});
// });

beforeEach(function(done){

	console.log('before Each');
	req = {};
	res = {};
	next = function(err){
		console.log('===== NEXT ====');
		console.log(err.message);
	};
	done();

});


before(function(done){
	var sequelize = Model.Feedback;

	sequelize.sync().then(function(){
		done();
	}).catch(function(err){
		throw err;
	})
//done();
});

after(function(){

	console.log('after');

});


describe("AppController", function() {

	describe("Feedback", function(){

		it("should save a posted message to the database", function(done) {

			req.body = {comments : 'Hello, this is feedback from the unit test'}
			req.user = {id:1}
			res.redirect = sinon.spy();
			req.flash = sinon.spy();

			//Model.Feedback.sync({force: true}).then(function () {
			// controller.addFeedback(req, res, next).then(function(data){
			// 	console.log('DATA: ', data, ' :DATA')
			// 	doneAgain();
			// });
			//});
			done();
			expect(res.redirect.calledOnce).to.equal(false);
			//expect(req.flash.calledOnce).to.equal(true);
			
		});     
	});

	describe("GET Programme", function() {

		it("should respond when no week is provided", function() {
			var req,res,spy;

			req = res = {};
			req.params = {};
			req.user = {current_week : 1}
			spy = res.render = sinon.spy();

			controller.showProgramme(req, res);
			expect(spy.calledOnce).to.equal(true);
			var args = spy.args[0];
			expect(args[0]).to.equal('programme/week-1');
			expect(args[1].user).to.equal(req.user);
			expect(args[1].weekData.constructor).to.equal(Object);
		});     

		it("should respond when a valid week is provided", function() {
			var req,res,spy;

			req = res = {};
			req.params = {week:1};
			req.user = {}
			req.user.current_week = 10;
			spy = res.render = sinon.spy();

			controller.showProgramme(req, res);
			expect(spy.calledOnce).to.equal(true);
			var args = spy.args[0];
			expect(args[0]).to.equal('programme/week-1');
			expect(args[1].user).to.equal(req.user);
			expect(args[1].weekData.constructor).to.equal(Object);
		});   
	});


	describe("test private functions", function() {

		it("getWeekRange: will return an array of integers", function() {

			var weekRange = controller._private.getWeekRange(5);
			expect(weekRange).to.deep.equal([1,2,3,4]);

		});     


		it("getWeekData: will return an object with 3 properties", function() {

			var weekData = controller._private.getWeekData(3,5);
			expect(weekData.constructor).to.equal(Object);
			expect(weekData.current).to.equal(5);
			expect(weekData.active).to.equal(3);
			expect(weekData.past).to.deep.equal([1,2,3,4])

		});  

		it("getWeekData: will return a valid object when provided with only one parameter", function() {

			var weekData = controller._private.getWeekData(undefined,4);
			expect(weekData.constructor).to.equal(Object);
			expect(weekData.current).to.equal(4);
			expect(weekData.active).to.equal(4);
			expect(weekData.past).to.deep.equal([1,2,3])

		});  

  });

	
});
