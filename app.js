// application
require('newrelic');

var cluster = require('cluster'),
    numCPUs = require('os').cpus().length;


if(cluster.isMaster && ['production', 'development'].indexOf(process.env.NODE_ENV) > -1 ){
    for (var i = 0; i < numCPUs; i++) {
        cluster.fork();
    }
    cluster.on('exit', function (worker) {
        // Replace the dead worker,
        console.log('Worker %d died :(', worker.id);
        cluster.fork();
    });
}else{
    runApp(cluster.worker);
}

function runApp(worker){

    var express = require('express'),
        app = express(),
        setupHandlebars  = require('./app/utils/setupHandlebars.js')(app),
        setupPassport = require('./app/utils/setupPassport'),
        flash = require('connect-flash'),
        appRouter = require('./app/routers/appRouter.js')(express),
        session = require('express-session'),
        bodyParser = require('body-parser'),
        cookieParser = require('cookie-parser'),
        jsonParser = bodyParser.json(),
        glob = require('glob'),
        sequelize = require('./app/utils/sequelize'),
        connectRedis = require('connect-redis');

    var port = process.env.PORT || 3000

    // initalize redis with session store
    var RedisStore = connectRedis(session);

    app.use(cookieParser())

    app.use(session({
      secret: '4564f6s4fdsfdfd', 
      resave: false, 
      saveUninitialized: false,
      store: new RedisStore(),
      proxy: true // if you do SSL outside of node.
    }));

    //app.use(session({ secret: '4564f6s4fdsfdfd', resave: false, saveUninitialized: false }));

    app.use('/public', express.static(__dirname + '/public'))

    app.use(flash())
    app.use(function(req, res, next) {
        if(worker && worker.id){
            res.setHeader('process', process.pid);
            res.setHeader('worker', worker.id);
        }
        
        res.locals.errorMessage = req.flash('error');
        res.locals.infoMessage = req.flash('info');
        res.locals.successMessage = req.flash('success');
        next();
    });

    app.use(jsonParser)
    app.use(bodyParser.urlencoded({
      extended: true
    }))

    setupPassport(app)

    var routers = glob.sync('./app/routers/*.js');
    routers.forEach(function (router) {
        app.use('/', require(router)(express));
    });


    // start app
    app.listen(port)
    console.log('Server started on port ' + port)

    module.exports.getApp = app

}

//runApp();
