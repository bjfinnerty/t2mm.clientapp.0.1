var session = require('express-session'),
	sequelize = require('./app/utils/sequelize'),
	SequelizeStore = connectSessionSequelize(session.Store);

var sessionStore = new SequelizeStore({
    db: sequelize
 });

sessionStore.sync();
