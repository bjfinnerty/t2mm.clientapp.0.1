COPY users (id, password, child_name, child_dob, family_name, mothers_name, mothers_contact_no, fathers_name, fathers_contact_no, working_mother, working_father, maternity_leave, phone_model_1, phone_model_2, tablet_model_1, tablet_model_2, email, salt, "createdAt", "updatedAt", folder_name, activation_date) FROM stdin;
1	$2a$10$T6VP4Fmff/FLPNTOtEdloupLYb5LusKxKCov2b/gL7Jun6TfqMPU2	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	test@talktomemore.com	$2a$10$T6VP4Fmff/FLPNTOtEdlou	2016-01-28 11:07:46.376+00	2016-01-28 11:07:46.376+00	\N	\N
\.

COPY recording (id, user_id, start_time, end_time, file_name, duration, word_count, top_ten_words, top_ten_themed_words, transcribe_time, folder_name, file_part, total_split, "createdAt", "updatedAt") FROM stdin;
1	1	2016-01-17 22:43:19	2016-01-17 10:43:21	recording-20160117-224319.wav	2	8	no: 1,low: 1,oh: 1,	\N	\N	\N	0	1	2016-01-17 21:15:42.566+00	\N
2	1	2016-01-20 22:43:19	2016-01-20 10:43:21	recording-20160120-224319.wav	2	8	no: 1,low: 1,oh: 1,	\N	\N	\N	0	1	2016-01-20 21:17:06.378+00	\N
3	1	2016-01-24 22:07:17	2016-01-24 10:07:26	recording-20160124-220717.wav	9	23	hello: 2,washington: 1,with: 1,if: 1,a: 1,will: 1,do: 1,row: 1,badger: 1,we: 1,	\N	\N	\N	0	1	2016-01-25 21:51:54.626+00	\N
4	1	2016-01-24 22:43:19	2016-01-24 10:43:21	recording-20160124-224319.wav	2	10	no: 2,oh: 1,	\N	\N	\N	0	1	2016-01-25 21:52:03.626+00	\N
5	1	2016-01-25 21:40:12	2016-01-25 09:45:11	recording-20160125-214012.wav	299	924	of: 26,and: 17,to: 16,a: 15,for: 10,i: 9,was: 9,are: 8,as: 7,but: 7,so: 6	\N	\N	\N	0	1	2016-01-25 22:03:02.395+00	\N
6	1	2016-01-16 17:16:24	2016-01-16 05:16:32	recording-20160116-171624.wav	8	20	to: 1,frodo: 1,if: 1,a: 1,write: 1,my: 1,want: 1,bicycle: 1,i: 1,oh: 1,	\N	\N	\N	0	1	2016-01-25 22:03:26.567+00	\N
7	1	2016-01-18 21:03:44	2016-01-18 09:03:48	recording-20160118-210344.wav	4	13	hello: 1,old: 1,my: 1,darkness: 1,friend: 1,	\N	\N	\N	0	1	2016-01-25 22:03:36.736+00	\N
8	1	2016-01-18 21:04:13	2016-01-18 09:04:15	recording-20160118-210413.wav	2	3		\N	\N	\N	0	1	2016-01-25 22:03:40.418+00	\N
9	1	2016-01-19 19:17:48	2016-01-19 07:17:55	recording-20160119-191748.wav	7	21	has: 2,with: 1,bob: 1,so: 1,don't: 1,bad: 1,israel: 1,lawyer: 1,know: 1,live: 1,who: 1	\N	\N	\N	0	1	2016-01-25 22:04:01.797+00	\N
10	1	2016-01-23 11:21:57	2016-01-23 11:22:05	recording-20160123-112157.wav	8	20	to: 1,mm: 1,press: 1,test: 1,no: 1,a: 1,lady: 1,pretty: 1,first: 1,this: 1,	\N	\N	\N	0	1	2016-01-25 22:04:16.378+00	\N
11	1	2016-01-24 22:43:19	2016-01-24 10:43:21	recording-20160124-224319.wav	2	11	no: 2,oh: 1,	\N	\N	\N	0	1	2016-01-25 22:04:25.581+00	\N
12	1	2016-01-24 22:07:17	2016-01-24 10:07:26	recording-20160124-220717.wav	9	23	hello: 2,washington: 1,with: 1,if: 1,a: 1,will: 1,do: 1,row: 1,badger: 1,we: 1,	\N	\N	\N	0	1	2016-01-26 16:59:01.097+00	\N
13	1	2016-01-24 22:43:19	2016-01-24 10:43:21	recording-20160124-224319.wav	2	10	no: 2,oh: 1,	\N	\N	\N	0	1	2016-01-26 16:59:10.237+00	\N
14	1	2016-01-25 21:40:12	2016-01-25 09:45:11	recording-20160125-214012.wav	299	924	of: 26,and: 17,to: 16,a: 15,for: 10,i: 9,was: 9,as: 8,are: 8,but: 7,so: 6	\N	\N	\N	0	1	2016-01-26 17:09:21.243+00	\N
15	1	2016-01-16 17:16:24	2016-01-16 05:16:32	recording-20160116-171624.wav	8	20	to: 1,frodo: 1,if: 1,a: 1,write: 1,my: 1,want: 1,bicycle: 1,i: 1,oh: 1,	\N	\N	\N	0	1	2016-01-26 17:09:43.832+00	\N
16	1	2016-01-18 21:03:44	2016-01-18 09:03:48	recording-20160118-210344.wav	4	13	hello: 1,old: 1,my: 1,darkness: 1,friend: 1,	\N	\N	\N	0	1	2016-01-26 17:09:53.318+00	\N
17	1	2016-01-18 21:04:13	2016-01-18 09:04:15	recording-20160118-210413.wav	2	3		\N	\N	\N	0	1	2016-01-26 17:09:56.568+00	\N
18	1	2016-01-19 19:17:48	2016-01-19 07:17:55	recording-20160119-191748.wav	7	21	has: 2,with: 1,bob: 1,so: 1,don't: 1,bad: 1,israel: 1,lawyer: 1,know: 1,live: 1,who: 1	\N	\N	\N	0	1	2016-01-26 17:10:16.151+00	\N
19	1	2016-01-23 11:21:57	2016-01-23 11:22:05	recording-20160123-112157.wav	8	20	to: 1,mm: 1,press: 1,test: 1,no: 1,a: 1,lady: 1,pretty: 1,first: 1,this: 1,	\N	\N	\N	0	1	2016-01-26 17:10:30.972+00	\N
20	1	2016-01-24 22:43:19	2016-01-24 10:43:21	recording-20160124-224319.wav	2	11	no: 2,oh: 1,	\N	\N	\N	0	1	2016-01-26 17:10:40.281+00	\N
\.

--
-- PostgreSQL database dump complete
--