--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO users (id, password, child_name, child_dob, family_name, mothers_name, mothers_contact_no, fathers_name, fathers_contact_no, working_mother, working_father, maternity_leave, phone_model_1, phone_model_2, tablet_model_1, tablet_model_2, email, salt, "createdAt", "updatedAt", folder_name, activation_date) VALUES (1, '$2a$10$T6VP4Fmff/FLPNTOtEdloupLYb5LusKxKCov2b/gL7Jun6TfqMPU2', 'Testy Jr.', NULL, 'McTest', NULL, NULL, 'Testy', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'test@talktomemore.com', '$2a$10$T6VP4Fmff/FLPNTOtEdlou', '2016-01-28 11:07:46.376+00', '2016-01-28 12:28:25.749+00', NULL, '2016-01-16 00:00:00+00');
INSERT INTO users (id, password, child_name, child_dob, family_name, mothers_name, mothers_contact_no, fathers_name, fathers_contact_no, working_mother, working_father, maternity_leave, phone_model_1, phone_model_2, tablet_model_1, tablet_model_2, email, salt, "createdAt", "updatedAt", folder_name, activation_date) VALUES (2, '$2a$10$.31UqgBZxgMfMpKwOnIeeOxthRHwGTc.mMM/PJ./w1MTTfdjf32YO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'oisintest@gmail.com', '$2a$10$.31UqgBZxgMfMpKwOnIeeO', '2016-02-01 20:56:44.536+00', '2016-02-01 20:56:44.536+00', NULL, NULL);
INSERT INTO users (id, password, child_name, child_dob, family_name, mothers_name, mothers_contact_no, fathers_name, fathers_contact_no, working_mother, working_father, maternity_leave, phone_model_1, phone_model_2, tablet_model_1, tablet_model_2, email, salt, "createdAt", "updatedAt", folder_name, activation_date) VALUES (3, '$2a$10$huhrHGKZ0VsWuT1x8RqSWOsOhf.ZpkbqDUpnclX2ureag2UW3EDKu', 'Zara Timmons', '2014-03-01', 'Timmons', 'Suzie Timmons', 851171913, 'Ross Timmons', 857705230, NULL, NULL, NULL, 'Apple i Phone', 'Apple i Phone', NULL, NULL, 'ross.timmons@gmail.com', '$2a$10$huhrHGKZ0VsWuT1x8RqSWO', '2016-02-15 10:56:44.62+00', '2016-02-15 10:57:35.282+00', NULL, '2016-02-15 00:00:00+00');
INSERT INTO users (id, password, child_name, child_dob, family_name, mothers_name, mothers_contact_no, fathers_name, fathers_contact_no, working_mother, working_father, maternity_leave, phone_model_1, phone_model_2, tablet_model_1, tablet_model_2, email, salt, "createdAt", "updatedAt", folder_name, activation_date) VALUES (4, '$2a$10$L279XTUYJeHA.7JKM/zrRem/PDZpUVeNMzQuXfed.lndNKJc6YuUS', 'Ryan', '2015-12-23', 'Foley', 'Fiona', 877407770, 'Mark', 868389420, NULL, NULL, NULL, 'iPhone 5', 'iPhone 6', 'iPad Air', NULL, 'marcofolo@gmail.com', '$2a$10$L279XTUYJeHA.7JKM/zrRe', '2016-02-16 09:27:12.049+00', '2016-02-16 09:28:44.936+00', NULL, '2016-02-16 00:00:00+00');
INSERT INTO users (id, password, child_name, child_dob, family_name, mothers_name, mothers_contact_no, fathers_name, fathers_contact_no, working_mother, working_father, maternity_leave, phone_model_1, phone_model_2, tablet_model_1, tablet_model_2, email, salt, "createdAt", "updatedAt", folder_name, activation_date) VALUES (5, '$2a$10$4lQtxyk2.gRXsnW23jEmQePfDiy5KCqtcuurMwv5rhxY3aW63aPkG', 'Jack', '2014-12-21', 'Spencer', 'Denise', 879165151, 'John', 877578685, NULL, NULL, NULL, 'Iphone', 'Iphone', 'Ipad', NULL, 'Spencer9t9@yahoo.co.uk', '$2a$10$4lQtxyk2.gRXsnW23jEmQe', '2016-02-17 22:11:42.999+00', '2016-02-17 22:13:48.741+00', NULL, '2016-02-17 00:00:00+00');
INSERT INTO users (id, password, child_name, child_dob, family_name, mothers_name, mothers_contact_no, fathers_name, fathers_contact_no, working_mother, working_father, maternity_leave, phone_model_1, phone_model_2, tablet_model_1, tablet_model_2, email, salt, "createdAt", "updatedAt", folder_name, activation_date) VALUES (6, '$2a$10$Xc0MumzBRyYm9G7KEoVvAOVRJ.gINkmZ1S8GJ9rCd5pOg33MZ2YeC', 'Lucy Curry', '2015-09-11', 'Curry', 'Lisa Hurley', 860367726, NULL, 860367726, NULL, NULL, NULL, 'iPhone 6', NULL, 'iPad Mini', NULL, 'lisa.c.hurley06@gmail.com', '$2a$10$Xc0MumzBRyYm9G7KEoVvAO', '2016-02-21 20:07:55.602+00', '2016-02-21 20:09:53.095+00', NULL, '2016-02-21 00:00:00+00');
INSERT INTO users (id, password, child_name, child_dob, family_name, mothers_name, mothers_contact_no, fathers_name, fathers_contact_no, working_mother, working_father, maternity_leave, phone_model_1, phone_model_2, tablet_model_1, tablet_model_2, email, salt, "createdAt", "updatedAt", folder_name, activation_date) VALUES (7, '$2a$10$/Ij7xntZ8RMKEuHSDJbkCO.UvT2ur3EFZmjvJeqN6WmHKobMJnrK6', 'Lily Smullen ', '2015-09-09', 'Smullen', 'Eilis O Leary ', 857383422, 'John Smullen ', 857349416, NULL, NULL, NULL, 'iPhone 6', NULL, 'IPad ', NULL, 'eilis.oleary@griffith.ie', '$2a$10$/Ij7xntZ8RMKEuHSDJbkCO', '2016-02-22 12:41:40.884+00', '2016-02-22 12:45:17.941+00', NULL, '2016-02-22 00:00:00+00');


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('users_id_seq', 7, true);


--
-- PostgreSQL database dump complete
--
