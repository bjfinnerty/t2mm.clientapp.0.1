#!/usr/bin/env bash

source env/bin/activate;
NODE_ENV="local" nodemon --debug app.js;